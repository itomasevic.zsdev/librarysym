<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200617100049 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE book (id INT AUTO_INCREMENT NOT NULL, copies INT NOT NULL, title VARCHAR(255) NOT NULL, short_about VARCHAR(512) NOT NULL, long_about VARCHAR(2048) NOT NULL, author VARCHAR(255) NOT NULL, available TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE rents DROP FOREIGN KEY FK_89DE39DD16A2B381');
        $this->addSql('ALTER TABLE rents ADD CONSTRAINT FK_89DE39DD16A2B381 FOREIGN KEY (book_id) REFERENCES book (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE rents DROP FOREIGN KEY FK_89DE39DD16A2B381');
        $this->addSql('DROP TABLE book');
        $this->addSql('ALTER TABLE rents DROP FOREIGN KEY FK_89DE39DD16A2B381');
        $this->addSql('ALTER TABLE rents ADD CONSTRAINT FK_89DE39DD16A2B381 FOREIGN KEY (book_id) REFERENCES books (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
    }
}
