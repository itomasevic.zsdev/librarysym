<?php


namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class UserController
{
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var Environment
     */
    private $environment;

    public function __construct(UserRepository $userRepository, Environment $environment)
    {

        $this->userRepository = $userRepository;
        $this->environment = $environment;
    }
    /**
     * @Route("/users/", name="library_books_users")
     */
    public function users()
    {
        $users = $this->userRepository->findBy([], ["email" => "ASC"]);

        $html = $this->environment->render(
            "users/index.html.twig",
            [
                "users" => $users
            ]
        );

        return new Response($html);
    }

    /**
     * @Route("/users/{id}", name="library_books_user")
     */
    public function user(User $user)
    {
        $html = $this->environment->render(
            "users/user.html.twig",
            [
                "user" => $user
            ]
        );

        return new Response($html);
    }
}