<?php


namespace App\Controller;


use App\Entity\Rent;
use App\Entity\User;
use App\Form\FinishType;
use App\Form\RentType;
use App\Repository\BookRepository;
use App\Repository\RentRepository;
use App\Repository\UserRepository;
use App\Service\changeAvailable;
use App\Service\isAvailable;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;


/**
 * @Route("/")
 */
class BookController extends AbstractController
{
    /**
     * @var \Twig\Environment
     */
    private $twig;
    /**
     * @var BookRepository
     */
    private $bookRepository;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var RouterInterface
     */
    private $router;
    /**
     * @var FlashBagInterface
     */
    private $flashBag;
    /**
     * @var RentRepository
     */
    private $rentRepository;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var isAvailable
     */
    private $isAvailable;
    /**
     * @var changeAvailable
     */
    private $changeAvailable;


    public function __construct(
        \Twig\Environment $twig,
        BookRepository $bookRepository,
        EntityManagerInterface $entityManager,
        RouterInterface $router,
        FlashBagInterface $flashBag,
        RentRepository $rentRepository,
        UserRepository $userRepository,
        isAvailable $isAvailable,
        changeAvailable $changeAvailable
    )
    {
        $this->twig = $twig;
        $this->bookRepository = $bookRepository;
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->flashBag = $flashBag;
        $this->rentRepository = $rentRepository;
        $this->userRepository = $userRepository;
        $this->isAvailable = $isAvailable;
        $this->changeAvailable = $changeAvailable;
    }

    /**
     * @Route("/", name="library_books_index")
     */
    public function index()
    {
        $books = $this->bookRepository->findBy([], ["title" => "ASC"]);

        $html = $this->twig->render(
            "books/index.html.twig",
            [
                "books" => $books,
            ]
        );

        return new Response($html);
    }

    /**
     * @Route("/rent", name="library_books_rent")
     */
    public function rent(Request $request)
    {
        $rent = new Rent();
        $form = $this->createForm(RentType::class, $rent);
        $form->handleRequest($request);

        if($this->rentRepository->findOneBy(["user" => $rent->getUser(), "book" => $rent->getBook()]))
        {
            $this->flashBag->add("notice", "Book already rented!");
            return new RedirectResponse($this->router->generate("library_books_rent"));
        }

        if($form->isSubmitted() && $form->isValid()){
            if(!$this->isAvailable->isAvailable($rent->getBook())) {
                $this->flashBag->add("notice", "Out of stock!");
                return new RedirectResponse($this->router->generate("library_books_rent"));
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($rent);
            $entityManager->flush();

            if(!$this->isAvailable->isAvailable($rent->getBook())){
                $this->changeAvailable->notAvailable($rent->getBook());
            }

            $this->flashBag->add("notice", "Successfully rented!");

            return new RedirectResponse($this->router->generate("library_books_index"));
        }

        return $this->render("books/rent.html.twig", [
            "form" => $form->createView()
        ]);
    }

    /**
     * @Route("/finish/", name="library_books_finish")
     */
    public function finish(Request $request)
    {
        $rent = new Rent();
        $form = $this->createForm(FinishType::class, $rent);
        $form->handleRequest($request);
        $rentBook = $rent->getBook();
        $rentUser = $rent->getUser();


        if($form->isSubmitted() && $form->isValid()) {
            if(!$this->rentRepository->findOneBy(["book" => $rentBook, "user" => $rentUser])){
                $this->flashBag->add("notice", "The selected user has not rented the book!");
                return new RedirectResponse($this->router->generate("library_books_finish"));
            }
            $rent = $this->rentRepository->findOneBy(["book" => $rentBook, "user" => $rentUser]);
            $this->entityManager->remove($rent);
            $this->entityManager->flush();
            $this->flashBag->add("notice", "Book successfully returned!");
            if($this->isAvailable->isAvailable($rentBook)){
                $this->changeAvailable->available($rentBook);
            }
            return new RedirectResponse($this->router->generate("library_books_index"));
        }

        return $this->render("books/finish.html.twig", [
            "form" => $form->createView()
        ]);
    }
}
