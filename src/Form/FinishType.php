<?php


namespace App\Form;


use App\Entity\Book;
use App\Entity\Rent;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FinishType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add("user", EntityType::class, [
            "class" => User::class,
            "choice_label" => "email",
            "placeholder" => "Choose a user"
        ])

            ->add("book", EntityType::class, [
                "class" => Book::class,
                "choice_label" => "title",
                "placeholder" => "Choose a book"

            ])
            ->add("Finish", SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            "data_class" => Rent::class
        ]);
    }
}