<?php

namespace App\DataFixtures;

use App\Entity\Book;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }
    public function load(ObjectManager $manager)
    {
        $this->loadBooks($manager);
        $this->loadUsers($manager);
    }

    private function loadBooks(ObjectManager $manager)
    {
        $book = new Book();
        $book->setTitle("Lord Of The Rings");
        $book->setCopies(3);
        $book->setShortAbout("The Lord of the Rings is an epic[1] high-fantasy novel written by English author and scholar J. R. R. Tolkien. The story began as a sequel to Tolkien's 1937 fantasy novel The Hobbit, but eventually developed into a much larger work. Written in stages between 1937 and 1949, The Lord of the Rings is one of the best-selling novels ever written, with over 150 million copies sold.[2] "
        );
        $book->setLongAbout("The Lord of the Rings is an epic[1] high-fantasy novel written by English author and scholar J. R. R. Tolkien. The story began as a sequel to Tolkien's 1937 fantasy novel The Hobbit, but eventually developed into a much larger work. Written in stages between 1937 and 1949, The Lord of the Rings is one of the best-selling novels ever written, with over 150 million copies sold.[2]

The title of the novel refers to the story's main antagonist, the Dark Lord Sauron,[a] who had in an earlier age created the One Ring to rule the other Rings of Power as the ultimate weapon in his campaign to conquer and rule all of Middle-earth. From quiet beginnings in the Shire, a hobbit land not unlike the English countryside, the story ranges across Middle-earth, following the course of the War of the Ring through the eyes of its characters, most notably the hobbits Frodo, Sam, Merry and Pippin. ");
        $book->setAuthor("J. R. R. Tolkien");
        $manager->persist($book);

        $book = new Book();
        $book->setTitle("Hobbit");
        $book->setCopies(2);
        $book->setShortAbout("Short about...");
        $book->setLongAbout("Long about..");
        $book->setAuthor("J. R. R. Tolkien");
        $manager->persist($book);

        $book = new Book();
        $book->setTitle("A Game of Thrones");
        $book->setCopies(3);
        $book->setShortAbout("a");
        $book->setLongAbout("b");
        $book->setAuthor("George R. R. Martin");
        $manager->persist($book);

        $manager->flush();
    }

    private function loadUsers(ObjectManager $manager)
    {
        $user = new User();
        $user->setFullName("Ilan Tomasevic");
        $user->setEmail("ilan.tomasevic@gmail.com");
        $manager->persist($user);
        $manager->flush();
    }
}
