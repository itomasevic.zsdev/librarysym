<?php


namespace App\Service;

use App\Repository\BookRepository;
use Doctrine\ORM\EntityManagerInterface;

class changeAvailable
{
    /**
     * @var BookRepository
     */
    private $bookRepository;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(BookRepository $bookRepository, EntityManagerInterface $entityManager)
    {

        $this->bookRepository = $bookRepository;
        $this->entityManager = $entityManager;
    }

    public function notAvailable($bookId)
    {
        $book = $this->bookRepository->findOneBy(["id" => $bookId]);
        $book->setAvailable(false);
        $this->entityManager->persist($book);
        $this->entityManager->flush();
    }

    public function available($bookId)
    {
        $book = $this->bookRepository->findOneBy(["id" => $bookId]);
        $book->setAvailable(true);
        $this->entityManager->persist($book);
        $this->entityManager->flush();
    }
}