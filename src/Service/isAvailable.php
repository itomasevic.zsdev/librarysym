<?php


namespace App\Service;


use App\Repository\RentRepository;

class isAvailable
{
    /**
     * @var RentRepository
     */
    private $rentRepository;

    public function __construct(RentRepository $rentRepository)
    {

        $this->rentRepository = $rentRepository;
    }

    public function isAvailable($book)
    {
        $copies = $book->getCopies();
        $bookId = $book->getId();
        $value = $this->rentRepository->findBy(["book" => $bookId]);
        $recordsNumber = count($value);
        $calc = intval($copies-$recordsNumber);
        if($calc <= 1){
            return false;
        }else{
            return true;
        }
    }

}